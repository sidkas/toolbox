# AAI Compute

## SSH
---
> ### Info
    VPN ip: 10.7.0.35

> ### Open terminal

    ssh -i {path/to/private_key} user@hostaddress -p {port number}

> ### Create key and copy to remote host

    ssh-keygen -f {path/to/ecdsa_keys}  -t ecdsa -b 521 -C "{email or domain address}"
    ssh-copy-id -i {path/to/public_key} user@hostaddress

    example: 
    ssh-keygen -f ~/.ssh/aai_compute.cer  -t ecdsa -b 521
    ssh-copy-id -i ~/.ssh/aai_compute.cer.pub sid@10.7.0.35

> ### Add ssh config 
    config path: ~/.ssh/config

    Host remotehostname
        User remoteusername
        Hostname hostaddress
        Port 22
        IdentityFile path/to/private_key

    Easy login using: 
        ssh remotehostname

    Examlple:
        Host aai_compute
            User sid
            Hostname 10.7.0.35
            Port 22
            IdentityFile ~/.ssh/aai_compute.cer

        ssh aai_compute

> ### Copy file/folder from or to remote
    download:
        scp -P {port number} -r -i {path/to/private_key} user@hostaddress:/path/to/remote_dir  /path/to/local_dir
    upload:
        scp -P {port number} -r -i {path/to/private_key} /path/to/local_dir user@hostaddress:/path/to/remote_dir  