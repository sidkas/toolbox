import numpy as np

print("Testing hello pyx")
import cython_examples.hello_world_cyt as hello_cyt
hello_cyt.fib(2000)

print("Testing Rectangle pyx-cpp")
import cython_examples.rectangle_cyt as rectangle_cyt
r = rectangle_cyt.RectangleCyt(2,3,4,5)
print("RectangleCyt => Area:", r.get_area())

import cython_examples.rectangle_user_cyt as rectangle_user_cyt
r_user = rectangle_user_cyt.RectangleUserCyt(r)
print("RectangleUserCyt => Area:", r_user.get_area2())

r_user.test_create()
print("RectangleUserCyt 2 => Area:", r_user.get_area2())
