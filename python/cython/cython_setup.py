from setuptools import setup
from setuptools.extension import Extension
from Cython.Build import cythonize

import numpy

include_dirs = [numpy.get_include(), 'cython_examples']

ext_hello_world = Extension(
    'cython_examples.hello_world_cyt',
    ['cython_examples/hello_world_cyt.pyx'],
)

ext_rect_cpp_example = Extension(
     'cython_examples.rectangle_cyt',
    ['cython_examples/rectangle_cyt.pyx'],
    include_dirs=include_dirs,
)

ext_usage_of_rectangle_functions = Extension(
     'cython_examples.rectangle_user_cyt',
    ['cython_examples/rectangle_user_cyt.pyx'],
)

cython_ext_modules = [
    ext_hello_world,
    ext_rect_cpp_example,
    ext_usage_of_rectangle_functions,
]

setup (
    name = "toolbox",
    ext_modules = cythonize(cython_ext_modules, compiler_directives={'language_level': "3"}),
    packages=['brasatt', 'brasatt.cython_extensions'],
)


