
cdef extern from "CRectangle.cpp":
    pass

# Declare the class with cdef
cdef extern from "CRectangle.h" namespace "shapes":
    cdef cppclass CRectangle:
        CRectangle() except +
        CRectangle(int, int, int, int) except +
        int x0, y0, x1, y1
        int getArea()
        void getSize(int* width, int* height)
        void move(int, int)