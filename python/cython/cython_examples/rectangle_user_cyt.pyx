
## distutils: language = c++

from cython_examples.rectangle_cyt cimport RectangleCyt

cdef class RectangleUserCyt:

    cdef RectangleCyt rect_cyt

    def __cinit__(self, the_ext_obj):
        self.rect_cyt = the_ext_obj

    cpdef test_create(self):
        self.rect_cyt = RectangleCyt(0,0,1,1)

    cpdef get_area2(self):
        return self.rect_cyt.get_area()
