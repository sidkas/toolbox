

cdef class RectangleCyt:

    def __init__(self, int x0, int y0, int x1, int y1):
        self.c_rect = CRectangle(x0, y0, x1, y1)

    cpdef double get_area(self):
        return self.c_rect.getArea()
