#ifndef RECTANGLE_H
#define RECTANGLE_H

namespace shapes {
    class CRectangle {
        public:
            int x0, y0, x1, y1;
            CRectangle();
            CRectangle(int x0, int y0, int x1, int y1);
            ~CRectangle();
            int getArea();
            void getSize(int* width, int* height);
            void move(int dx, int dy);
    };
}

#endif
