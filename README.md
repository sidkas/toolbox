# ToolBox

Curated tools and useful information.

## Table of contents

- [Git](#git)
- [SSH](#ssh)
- [VPN](#vpn)
- [PyEnv](#pyenv)
- [Docker](#docker)

## Git
- change to ssh authentication
    - ssh-keygen -f ~/.ssh/git.cer  -t ecdsa -b 521 -C "{email or domain address}"
    - add host in ssh config file (path: ~/.ssh/config)
        ```
        ----- example -----
        Host gitlab.com
            Hostname gitlab.com
            PreferredAuthentications publickey
            IdentityFile ~/.ssh/git.cer
        ```
    - copy public key (git.cer.pub) to the git hosting (github, gitlab ..) 
    - copy url from ssh clone
    - git remote set-url origin {copied ssh clone url}

## SSH
---
> ### Open terminal

    ssh -i {path/to/private_key} user@hostaddress -p {port number}

> ### Create key and copy to remote host

    ssh-keygen -f {path/to/ecdsa_keys}  -t ecdsa -b 521 -C "{email or domain address}"
    ssh-copy-id -i {path/to/public_key} user@hostaddress

> ### Remove password authentication

    config path: /etc/ssh/sshd_config 
  
    PermitRootLogin no
    ChallengeResponseAuthentication no
    PasswordAuthentication no
    UsePAM no

> ### Tunnel remote port to local port
    localhost:8080 on remote machine
    localhost:5000 on local machine
    foreground: 
        ssh -L localhost:5000:localhost:8080 user@hostaddress -p {port number}
    background: 
        ssh -f -N -T -L localhost:5000:localhost:8080 user@hostaddress -p {port number}
    autossh:
        autossh -M 0 -o "ServerAliveInterval 30" -o "ServerAliveCountMax 3"  -f -N -T -L localhost:5000:localhost:8080 user@hostaddress -p {port number}

> ### Forward local port to remote
    localhost:5000 on remote machine
    localhost:22 on local machine
    foreground: 
        ssh -R localhost:5000:localhost:22 user@hostaddress -p {port number}
    background: 
        ssh -f -N -T -R localhost:5000:localhost:22 user@hostaddress -p {port number}
    autossh:
        autossh -M 0 -o "ServerAliveInterval 30" -o "ServerAliveCountMax 3"  -f -N -T -R localhost:5000:localhost:22 user@hostaddress -p {port number}


> ### Add ssh config 
    config path: ~/.ssh/config

    Host remotehostname
        User remoteusername
        Hostname hostaddress
        Port 22
        IdentityFile path/to/private_key

    Easy login using: 
        ssh remotehostname


> ### Copy from or to remote
    download:
        scp -P {port number} -r -i {path/to/private_key} user@hostaddress:/path/to/remote_dir  /path/to/local_dir
    upload:
        scp -P {port number} -r -i {path/to/private_key} /path/to/local_dir user@hostaddress:/path/to/remote_dir  


**[`<< back to top >>`](#)**


## VPN

> ### WireGuard server
- install: https://github.com/Nyr/wireguard-install
- web ui: https://github.com/ngoduykhanh/wireguard-ui


> ### WireGuard client
- windows and mac: https://www.wireguard.com/install/
- linux
    ```
    sudo apt update -y
    sudo apt install wireguard openresolv
    sudo mkdir -p /etc/wireguard

    copy .conf to /etc/wireguard/wgazure.conf

    sudo wg-quick up wgazure
    sudo systemctl enable wg-quick@wgazure

    check public ip: curl ifconfig.me
    ```
- To stop routing all web traffic via vpn IP, 
change AllowedIPs = 0.0.0.0/0,::/0
to AllowedIPs = 10.7.0.0/24 (VPN server ip) .conf file


## WOL (wake on lan)
- https://www.techrepublic.com/article/how-to-enable-wake-on-lan-in-ubuntu-server-18-04/
- https://necromuralist.github.io/posts/enabling-wake-on-lan/
- https://www.cyberciti.biz/tips/linux-send-wake-on-lan-wol-magic-packets.html

## Wake on Wlan

- ...


## PyEnv

### conda 

- Install miniconda [bash script](pyenv/install_miniconda.sh) or [web](https://docs.conda.io/projects/conda/en/latest/user-guide/install/windows.html)
- Install requirements ```conda env create -f requirements.yml```
- Activate conda environment ```conda activate {env_name}```
- To update the env ```conda env update --name {env_name} --file requirements.yml```
- Remove env ```conda remove --name {env_name} --all ```

### venv
- Installation python vitualenv [script](pyenv/venv.sh)

- linux:
    - python3 -m venv {env_name} && source {env_name}/bin/activate
    - pip install  -r requirements.txt

- mac:
    - python3 -m venv {env_name} && source {env_name}/bin/activate
    - pip install  -r requirements.txt

- windows:
    - python3 -m venv {env_name}
    - {env_name}\Scripts\activate
    - pip install  -r requirements.txt

## Linux

### User management
- adduser newuser
- usermod -aG sudo username
- sudo passwd username

### Kill processes by pattern
- ```kill -9 $(pgrep -f 'somepattern')```

## tmux
- Split screen vertically: Ctrlb and Shift 5
- Split screen horizontally: Ctrlb and Shift "
- Toggle between panes: Ctrl b and o
- Close current pane: Ctrl b and x