echo ""
echo "### Wiregard-ui Services"
SYSTEMCTL_PATH="/usr/bin/systemctl"
WG_INTERFACE="wg0"

echo "[Unit]
Description=Restart WireGuard
After=network.target

[Service]
Type=oneshot
ExecStart=$SYSTEMCTL_PATH restart wg-quick@$WG_INTERFACE.service" > /etc/systemd/system/wgui.service

echo "[Unit]
Description=Watch /etc/wireguard/$WG_INTERFACE.conf for changes

[Path]
PathModified=/etc/wireguard/$WG_INTERFACE.conf

[Install]
WantedBy=multi-user.target" > /etc/systemd/system/wgui.path

$SYSTEMCTL_PATH enable wgui.{path,service}
$SYSTEMCTL_PATH start wgui.{path,service}